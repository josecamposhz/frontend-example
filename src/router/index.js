import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

const router = new Router({
    mode: 'history',
    
    routes: [
      {
        path: '/',
        component: () => import('../App'),
        children: [
          {
            path: '/login',
            name: 'Login',
            component: () => import('../views/Login')
          },
          {
            path: '/registro',
            name: 'Registro',
            component: () => import('../views/Registro')
          },
          {
            path: '/',
            name: 'CRUD',
            component: () => import('../views/CRUD')
          }
        ]
      },
      {
        path:'*',
        redirect: '/'
      }
    ]
})

router.beforeEach((to, from, next) => {
  if (localStorage.getItem('token') !== null) {
    if (to.path === '/login' || to.path === '/registro') {
      next({ path: '/' })
    } else next()
  } else {
    if (to.path === '/login' || to.path === '/registro') next()
    else next({ path: '/login' })
  }
})

export default router